using Microsoft.AspNetCore.Mvc;
using powerplant.Data.ViewModels;
using powerplant.Services;

namespace powerplant.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductionPlantController : ControllerBase
    {
        private readonly IProductionPlantService _productionPlantService;
        public ProductionPlantController(IProductionPlantService productionPlantService)
        {
            _productionPlantService = productionPlantService;
        }
        [HttpPost]
        public async Task<IActionResult> Post(Payload payload)
        {

            foreach (var powerPlant in payload.Powerplants)
            {
                var plantsParams = _productionPlantService.GetPlantParametre(powerPlant.Type, payload.Fuels, powerPlant.Efficiency, powerPlant.Pmax);

                powerPlant.Pt = plantsParams.Pt;

                powerPlant.MarginalCost = plantsParams.MarginalCost;
            }

            var result = payload.Powerplants.OrderBy(p => p.MarginalCost).ThenBy(p => p.Pmin).ThenByDescending(p => p.Pt).ToList();

            var responses = result.Where(x => x.MarginalCost == payload.Load);

            var res = new Response();

            foreach (var response in responses)
            {
                res.name = response.Name;
                res.p = response.Pt == 0 ? response.Pmax : response.Pt;
            };

            return Ok(res);
        }
    }
}