﻿using powerplant.Data.ViewModels;

namespace powerplant.Services
{
    public interface IProductionPlantService
    {
         void CalculateProduction(Payload payload);
        public PlantParameters GetPlantParametre(String type, Fuels fuels, float efficiency, float pMax);


    }
}
