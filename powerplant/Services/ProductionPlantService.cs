﻿using powerplant.Data.ViewModels;

namespace powerplant.Services
{
    public class ProductionPlantService : IProductionPlantService 
    {
        private const double C02RATIO = 0.3;
        public void CalculateProduction(Payload payload)
        {
            // 1.Determine the needed output
            //2.Determine the wind efficiency
            //3.Take all wind based powerplants and produce the wind efficiency % from their max output(PMax)
            //4.Take all gas turbines and produce their max imum until reach the needed output
            //5.If still neded more power, use the kerozine turbines to produce the rest of the needed power
            var response = new List<Result>();

            var neededOutput = payload.Load;
            var windEfficiency = payload.Fuels.Wind;
            var powerPlants = payload.Powerplants.OrderByDescending(p => p.Efficiency).ThenByDescending(p => p.Pmax);
            foreach(var powerPlant in powerPlants)
            {
                var result = new Result()
                { 
                    Name = powerPlant.Name,
                  //  P = powerPlant.
                };


            }


        }
        public PlantParameters GetPlantParametre(String type, Fuels fuels, float efficiency, float pMax)

        {
            var plantParameters = new PlantParameters();

            if (type.Equals("gasfired"))
            {
                if (fuels.Gas >= 0 && fuels.Co2 >= 0)
                {
                    //Taken into account that a gas-fired powerplant also emits CO2, the cost of running the powerplant should also take into account the cost of the emission allowances.
                    plantParameters.MarginalCost = (fuels.Gas / efficiency) + ((float)C02RATIO * fuels.Co2);
                    plantParameters.Pt = pMax;
                }
            }
            else if (type.Equals("turbojet"))
            {
                if (fuels.Kerosine >= 0)
                {
                    plantParameters.MarginalCost = (fuels.Kerosine / efficiency);
                    plantParameters.Pt = pMax;
                }
            }
            else if (type.Equals("windturbine"))
            {
                if (fuels.Wind >= 0)
                {
                    plantParameters.MarginalCost = 0;
                    plantParameters.Pt = (float)Math.Round((fuels.Wind * pMax / 100), 1);
                }
            }
            return plantParameters;
        }

        
      

    }
}
