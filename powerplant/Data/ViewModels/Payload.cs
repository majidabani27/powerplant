﻿using Newtonsoft.Json;

namespace powerplant.Data.ViewModels
{
    public class Payload
    {
        [JsonProperty("load")]
        public int Load { get; set; }

        [JsonProperty("fuels")]
        public Fuels Fuels { get; set; }

        [JsonProperty("powerplants")]
        public List<PowerPlants> Powerplants { get; set; }
    }
}
