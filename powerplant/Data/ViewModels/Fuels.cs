﻿using Newtonsoft.Json;

namespace powerplant.Data.ViewModels
{
    public class Fuels
    {
        [JsonProperty("gas")]
        public float Gas { get; set; }

        [JsonProperty("kerosine")]
        public float Kerosine { get; set; }

        [JsonProperty("co2")]
        public float Co2 { get; set; }

        [JsonProperty("wind")]
        public float Wind { get; set; }
    }
}
