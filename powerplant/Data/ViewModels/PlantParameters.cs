﻿namespace powerplant.Data.ViewModels
{
    public class PlantParameters
    {
        public float MarginalCost { get; set; }
        public float Pt { get; set; }
    }
}
