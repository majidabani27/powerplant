﻿using Newtonsoft.Json;

namespace powerplant.Data.ViewModels
{
    public class PowerPlants
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("efficiency")]
        public float Efficiency { get; set; }

        [JsonProperty("pmin")]
        public int Pmin { get; set; }

        [JsonProperty("pmax")]
        public int Pmax { get; set; }

        [JsonIgnore]
        public float MarginalCost { get; set; }
        [JsonIgnore]
        public float Pt { get; set; }

        public PowerPlants()
        {
            MarginalCost = 0;

            Pt = 0;
        }
    }
}
